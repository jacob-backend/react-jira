module.exports = (req, res, next) => {
  if (req.method === "POST" && req.path === "/login") {
    if (req.body.username === "jacob" && req.body.password === "123456") {
      return res.status(200).json({
        user: {
          id: 1,
          name: "jacob",
          email: "jacob.zhou@gmail.com",
          title: "Mr",
          organization: "Accenture Dev Team",
          token: "123",
        },
      });
    } else {
      return res.status(400).json({
        message: "Invalid Credentials",
      });
    }
  }
  if (req.method === "GET" && req.path === "/me") {
    if (req.headers.authorization === "Bearer 123") {
      return res.status(200).json({
        user: {
          id: 1,
          name: "jacob",
          email: "jacob.zhou@gmail.com",
          title: "Mr",
          organization: "Accenture Dev Team",
          token: "123",
        },
      });
    } else {
      return res.status(401).json({
        message: "Authorization error",
      });
    }
  }
  next();
};
