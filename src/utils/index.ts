import { useState, useEffect } from "react";
export const isFalsy = (value: unknown) => (value === 0 ? false : !value);
export const isVoid = (value: unknown) =>
  value === undefined || value === null || value === "";

// Remove empty value - key/value pairs in object
export const cleanObject = (object: { [key: string]: unknown }) => {
  const result = { ...object };

  Object.keys(result).forEach((key) => {
    const value = result[key];
    if (isVoid(value)) {
      delete result[key];
    }
  });
  return result;
};

export const useMount = (callback: () => void) => {
  useEffect(() => {
    callback();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};

// Here as the input value type is random unknown,
// So we set it to unknown, this is better than any, but
// TS will consider the return value also unknown type
// However, the unknown type value can not be assign to any other param
//
// So the solution we want here is to connect the value and the return
// value with same type, so what we need to use is the
// -- Generics -- <T> fanxing
export const useDebounce = <T>(value: T, delay?: number) => {
  const [debouncedVal, setDebouncedVal] = useState(value);

  useEffect(() => {
    const timeout = setTimeout(() => setDebouncedVal(value), delay);
    return () => clearTimeout(timeout);
  }, [value, delay]);

  return debouncedVal;
};

export const resetRoute = () => (window.location.href = window.location.origin);
