import { useAuth } from "context/auth-context";
import qs from "qs";
import { useCallback } from "react";
import * as auth from "../auth-provider";
const apiUrl = process.env.REACT_APP_API_URL;

interface Config extends RequestInit {
  token?: string;
  data?: object;
}
export const http = async (
  endpoint: string,
  { data, token, headers, ...customConfig }: Config = {}
) => {
  const config = {
    method: "GET",
    headers: {
      Authorization: token ? `Bearer ${token}` : "",
      "Content-Type": data ? "application/json" : "",
    },
    ...customConfig,
  };

  if (config.method.toUpperCase() === "GET") {
    endpoint += `?${qs.stringify(data)}`;
  } else {
    config.body = JSON.stringify(data || {});
  }
  return window
    .fetch(`${apiUrl}/${endpoint}`, config)
    .then(async (response) => {
      if (response.status === 401) {
        await auth.logout();
        window.location.reload();
        return Promise.reject({ message: "Please login in again" });
      }
      const data = await response.json();
      if (response.ok) {
        return data;
      } else {
        return Promise.reject(data);
      }
    });
};

export const useHttp = () => {
  const { user } = useAuth();

  // return ([endpoint, config]: [string, Config]) => http(endpoint, {...config, token: user?.token})
  // TODO: TS operators
  const httpCallback = useCallback(
    (...[endpoint, config]: Parameters<typeof http>) =>
      http(endpoint, { ...config, token: user?.token }),
    [user]
  );
  return httpCallback;
};

// type Person = {
//     name: string,
//     age: number,
//     what: number
// }
// type newPerson = Omit<Person, "name"|"age">;

// const p0: Partial<Person> = {name:'z3', age: 8} //ok
// const p1: Partial<Person> = {age: 8} //ok
// const p2: Partial<Person> = {} //ok
// const p3: Partial<Person> = {age: 'hi'} //err
// const p4: Partial<Person> = {what: 'hi'} //err
